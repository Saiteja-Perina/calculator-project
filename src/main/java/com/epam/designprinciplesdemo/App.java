package com.epam.designprinciplesdemo;
/**
 * Hello world!
 *
 */
import java.util.*;

class Calculator{
	int add(int i, int j) {
		return i+j;
	}
	int subtract(int i, int j) {
		return i-j;
	}
	int multiply(int i, int j) {
		return i*j;
	}
	double divide(int i, int j) {
		return (double)i/j;
	}
}

public class App 
{
    public static void main( String[] args )
    {
    	int a,b;
    	Scanner sc = new Scanner(System.in);
    	a=sc.nextInt();
    	b=sc.nextInt();
    	Calculator cal = new Calculator();
    	System.out.println(cal.add(a, b));
    	System.out.println(cal.subtract(a, b));
    	System.out.println(cal.multiply(a, b));
    	System.out.println(cal.divide(a, b));
    }
}
